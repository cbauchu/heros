

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;


public class PanelFilsDeuxLabels extends JPanel {

	public PanelFilsDeuxLabels() {
	
		this.setLayout(new GridLayout(2,1,8,8));
		

		JPanel panelPersonnages = new PanelPerso();
		JPanel panelCombat = new PanelCombat();
		
		
		this.add(panelPersonnages);
		this.add(panelCombat);
		
	}  // constructeur
}  // classe
