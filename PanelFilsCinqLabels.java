

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import java.awt.BorderLayout;


public class PanelFilsCinqLabels extends JPanel {

	public PanelFilsCinqLabels() {
	
		setLayout(new BorderLayout(20,20)) ;
		
		JLabel labelNord = new JLabel("Zone Nord", JLabel.CENTER);
		JLabel labelSud = new JLabel("Zone Sud", JLabel.CENTER);
		
		labelNord.setOpaque(true);
		labelSud.setOpaque(true);


		labelNord.setFont(new Font("Arial", Font.BOLD,20));
		labelSud.setFont(new Font("Proxima nova", Font.PLAIN,22));
		

		add(labelNord, BorderLayout.NORTH);
		add(labelSud, BorderLayout.SOUTH);

		
		setBackground(new Color(100,67,98));
	}  // constructeur
}  // classe
