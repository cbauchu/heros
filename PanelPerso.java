import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JRadioButton;
import java.awt.GridLayout;


public class PanelPerso extends JPanel {

	public PanelPerso() {
		Heros heros1 = new Heros("Lousi", "belette", "f");
		Heros heros2 = new Heros("Lousi", "belette", "m");
	
		this.setLayout(new GridLayout(1,2,8,8));
		

		JPanel panelHeros1 = new PanelHeros1(heros1);
		JPanel panelHeros2 = new PanelHeros1(heros2);
	
	
		this.add(panelHeros1);
		this.add(panelHeros2);
	}
}

