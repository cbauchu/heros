public class Heros {
	private static String chNom;
	private String chClasse;
	private String chGenre;
	private int chPDVInitial;
	public static float chPointsDeVie;
	private float chAttaque;
	public int chDefense;
	private int chDommages;
	
	
	
	public String toString() {
		if (chGenre == "f"){
			return chNom +" la "+ chClasse;}
		else{
			return chNom +" le "+ chClasse;
		}
		
	}
	
	public Heros (String parNom, String parClasse, String parGenre){
		chNom=parNom;
		chClasse=parClasse;
		chGenre=parGenre;
		
		if (this.chClasse == "Tank"){
		this.chPDVInitial=300;
		this.chPointsDeVie=300;
		this.chAttaque=60;
		this.chDefense=60;
		}
		
		if (this.chClasse == "Mage"){
		this.chPDVInitial=70;
		this.chPointsDeVie=70;
		this.chAttaque=90;
		this.chDefense=50;
		}
		
		if (this.chClasse == "Soigneur"){
		this.chPDVInitial=70;
		this.chPointsDeVie=70;
		this.chAttaque=90;
		this.chDefense=50;
		}
		
		
		
		else{
		chPointsDeVie=100;
		chPDVInitial=100;
		chAttaque=10;
		chDefense=10;
		chDommages=10;
		}
	}
	
	public Heros (String parNom, String parClasse, String parGenre, int parPointsDeVie, int parAttaque, int parDefense, int parDommages){
		chNom=parNom;
		chClasse=parClasse;
		chGenre=parGenre;
		chPointsDeVie=parPointsDeVie;
		chPDVInitial=parPointsDeVie;
		chAttaque=parAttaque;
		chDefense=parDefense;
		chDommages=parDommages;
	}
	
	public boolean estVivant(){
		if(this.chPointsDeVie>0){
		return true;
		}
		
		else {
		return false;
		}
	}
	
	public String estVivantaffichage(){
		if (!estVivant()){
			System.out.println(this.chPointsDeVie);
			return this + " est mort ! \n Kamoulox";
		}
		else{
			return "Pv restant de "+ this + ": " + this.chPointsDeVie;
		}
	}
	

	public void Soigner() {
		this.chPointsDeVie=this.chPointsDeVie + (float) 0.5*(this.chPDVInitial);
		System.out.println(this.chNom + " se soigne et retrouve toute sa santé : " + this.chPDVInitial + " points de vie.");
	}
	
	public void Attaquer (Heros parHeros) {
		float p = (float) this.chAttaque/(this.chAttaque+parHeros.chDefense);
		float proba = (float) 0.05;
		double reussite = Math.random();
		double reussite2 = Math.random();
		float atqcritique= (float) 1.5*(this.chAttaque);
		
		if(reussite<p){
			
			 if(reussite2<=proba){
			parHeros.chPointsDeVie=parHeros.chPointsDeVie -  atqcritique;
			System.out.println(this + " inflige un coup critique ! " + atqcritique +"point de degats");
			}
			
			else{
				parHeros.chPointsDeVie=parHeros.chPointsDeVie - this.chAttaque;
				System.out.println(this + " attaque " + parHeros + " et lui inflige " + this.chAttaque + " dégâts.");
			}
		}
		
		else{
			System.out.println(this + " attaque " + parHeros + " et rate");
			
		}
	}
	
	public void setNom(String parNom){
		chNom=parNom;
	}
	
	public void setClasse(String parClasse){
		chClasse=parClasse;
	}
	
	public void setGenre(String parGenre){
		chGenre=parGenre;
	}
	
	public static String getNom(){
		return this.chNom;
	}
	
	public static float getPointsDeVie() {
		return this.chPointsDeVie;
	}
}
