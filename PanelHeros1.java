import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JRadioButton;


public class PanelHeros1 extends JPanel implements ActionListener {

	JButton bouton = new JButton("Envoi");
	JTextField zoneNom = new JTextField ("Lousi");
	private JRadioButton jr1 = new JRadioButton("m");
	private JRadioButton jr2 = new JRadioButton("f");
	Heros heros = new Heros("Lousi", "belette", "f");
	int check = 0;
	
	public PanelHeros1(Heros parHeros) {
		
		add (zoneNom);
		
		String[] choix = {"Belette", "Mammouth"};
		JComboBox MenuClasse = new JComboBox(choix);
		add(MenuClasse);
		MenuClasse.addItemListener(new ItemState());
		
		jr1.addActionListener(new StateListener());
		jr2.addActionListener(new StateListener());
		add(jr1);
		add(jr2);
		
		add(bouton);
		bouton.addActionListener(this);
		
		
	}  // constructeur
	
	class ItemState implements ItemListener{
		public void itemStateChanged(ItemEvent e) {
			String tempClasse = (String) e.getItem();
			heros.setClasse(tempClasse);
		}
	}
	
	public void actionPerformed(ActionEvent parEvt) {
		
		if((parEvt.getSource() == bouton)&&(check == 0)){
			String chaineNom=zoneNom.getText();
			heros.setNom(chaineNom);
			System.out.println(heros);
			check=1;
			
		}
	}
	
	class StateListener implements ActionListener{
		public void actionPerformed(ActionEvent f) {
			heros.setGenre(((JRadioButton)f.getSource()).getText());
		}
	}
}
